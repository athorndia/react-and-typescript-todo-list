import { useState } from "react";

export interface Todo {
  id: string;
  text: string;
  completed: boolean;
}

export function useTodos() {
  const [todos, setTodos] = useState<Todo[]>([]);

  function generateId(): string {
    return Date.now().toString();
  }

  function addTodo(text: string) {
    const todo: Todo = {
      id: generateId(),
      text,
      completed: false,
    };

    setTodos([...todos, todo]);
  }

  function toggleTodoComplete(id: string) {
    setTodos(
      todos.map((todo) =>
        todo.id === id ? { ...todo, completed: !todo.completed } : todo
      )
    );
  }

  return { todos, addTodo, toggleTodoComplete };
}
