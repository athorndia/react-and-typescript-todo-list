import styled from "@emotion/styled";
import { useTodos } from "../hooks/todos";
import { TodoItem } from "./TodoItem"; // import TodoItem

export function TodoList() {
  const { todos, toggleTodoComplete } = useTodos();

  return (
    <StyledTodoList>
      {todos.map((todo) => (
        <TodoItem
          key={todo.id}
          id={todo.id}
          label={todo.text}
          checked={todo.completed}
          onChange={() => toggleTodoComplete(todo.id)}
        />
      ))}
    </StyledTodoList>
  );
}

const StyledTodoList = styled.ul({
  width: "100%",
  listStyle: "none",
});

/*
export function TodoList() {
  const { todos, toggleTodoComplete } = useTodos();

  return (
    <StyledTodoList>
      {todos.map((todo) => (
        <li key={todo.id}>
          <span onClick={() => toggleTodoComplete(todo.id)}>
            {todo.completed ? "✓" : ""} {todo.text}
          </span>
        </li>
      ))}
    </StyledTodoList>
  );
}

const StyledTodoList = styled.ul({
  width: "100%",
  listStyle: "none",
});*/
