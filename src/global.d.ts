declare module "*.svg" {
  const content: string | boolean; // Changed from 'any' to 'string | boolean' or 'unknown'

  export default content;
}

interface Todo {
  id: string;
  label: string;
  checked: boolean;
}
